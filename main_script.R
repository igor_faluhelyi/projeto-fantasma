#______Projeto Fantasma_____#
#__________Igor_____________#

##### Pacotes, diretorio, STAT theme e banco de dados ----

## Pacotes

#install.packages('tidyverse')
#install.packages('pacman')
#install.packages('labelVector')

library(pacman)
library(labelVector)
library(scales)
library(vcd)

pacman::p_load(tidyverse, lubridate, readxl, janitor) #pacotes

## diretorio

setwd("C:/Users/Igor/Desktop/ESTAT/Projeto Fantasma") #diretorio

## STAT theme

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat2),
      scale_colour_manual(values = cores_estat2)
    )
  )
}

cores_estat1 <- c('#A11D21','#663333','#FF6600','#CC9900',
                 '#CC9966','#999966','#006606','#008091',
                 '#003366','#041835','#666666')

cores_estat2 <- c('#A11D21', '#003366', '#CC9900')


## Banco de dados
data <- read_excel("Banco de dados - Marcas Internacionais.xlsx",
           sheet='Data') #import dos dados


#limpeza e ajustes finos em data
data <- data[c(-1,-2),]
data <- janitor::row_to_names(data,1)

data$Age <- as.numeric(data$Age)

data$"Q7_1 - Daring" <- as.double(data$"Q7_1 - Daring")

data$`Q2: importance of decision` <- as.numeric(data$`Q2: importance of decision`)
data$`Q4: Risk in case of wrong decision`<- as.numeric(
  data$`Q4: Risk in case of wrong decision`)

data$Involvement <- as.numeric(data$Involvement)
data$`Satisfaction - ACSI` <- as.numeric(data$`Satisfaction - ACSI`)

data$Category <- factor(data$Category)
levels(data$Category) <- c(
  "Beauty products",
  "Home design and decoration",
  "Household Products (cleaning ingredients etc.)",
  "Media and entertainment",
  "Sports and hobbies",
  "Technology products and stores",
  "Telecommunications",
  "Travel services",
  "Beverages",
  "Cars",
  "Children's products",
  "Clothing products",
  "Department stores",
  "Financial services",
  "Food and dining",
  "Health products and services"
)

data$`Type of good` <- factor(data$`Type of good`)
levels(data$`Type of good`) <- c("Search", "Experience", "Credence")

data$Premium <- factor(data$Premium)
levels(data$Premium) <- c("Premium", "Middle", "Value")

data$Internet <- factor(data$Internet)
levels(data$Internet) <- c("Nao", "Sim")

data$`Interbrand top list` <- factor(data$`Interbrand top list`)
levels(data$`Interbrand top list`) <- c("Nao", "Sim")

data$Visibility <- as.numeric(data$Visibility)

data$High_quality_pct <- as.numeric(data$High_quality_pct)

data$Product <- as.factor(data$Product)
levels(data$Product) <- c("Product", "Service", "Mix")

##### Analise do topico 1 ----

### Objetivo: Quantidade de marcas divulgadas em cada um dos anos ate agosto de 2010

## Solucao 
data <- data %>% mutate(Age_dias = Age*365.25, data_lancamento =
                          as_date("20100801")-Age_dias)

tabela1 <- data %>% group_by(year(data_lancamento)) %>% summarise(freq = n())

marcas_lan_nesse_ano <- c()

for (i in tabela1$`year(data_lancamento)`){
  
  marcas_lan_nesse_ano <- c(marcas_lan_nesse_ano,data[year(data$data_lancamento) == i, 2])
}

tabela1 <- tabela1 %>% mutate(marcas_lan_nesse_ano = marcas_lan_nesse_ano) 

colnames(tabela1) <- c("Ano", "Quantidade de marcas divulgadas", "marcas divulgadas")

a <- cut(tabela1$Ano, 5, labels = c("1800 a 1850", "1850 a 1890",
                                    "1890 a 1930", "1930 a 1970",
                                    "1970 a 2010"))

tabela1 <- tabela1 %>% mutate(faixa_anos = a)

tabela1 #solucao final


## Grafico de linhas (grafico 1)
ggplot(tabela1) + aes(x=Ano, y=`Quantidade de marcas divulgadas`, group =1) +
  geom_line(size=1, colour ="#A11D21") + geom_point(colour ="#A11D21", size=2) + 
  labs(x="Ano", y="Quantidade de marcas divulgadas") + theme_estat()

#ggsave("fig_1.png", width = 158, height = 93, units = "mm", device = "png")

## Grafico de colunas (grafico 2)


ggplot(tabela1) +
  aes(x = faixa_anos, y = `Quantidade de marcas divulgadas`, label = faixa_anos) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) + 
  labs(x = "Faixa de 50 anos", y = "Quantidade de marcas divulgadas") +
  theme_estat()

#ggsave("fig_2.png", width = 158, height = 93, units = "mm", device = "png")

##### Analise do topico 2 ----

### Objetivo:
### Há algum tipo de relação significativa entre a idade das marcas e a avaliação de ousadia?

data <- data %>% mutate(Daring_rate = (data$"Q7_1 - Daring")/5)

## Dispersão (grafico 3)
ggplot(data) +
  aes(x = `Age`, y = `Q7_1 - Daring`) +
  geom_point(colour = "#A11D21", size = 3) +
  labs(
    x = "Idade das Marcas (anos)",
    y = "Avaliação de Ousadia"
    ) +
  theme_estat()
#ggsave("fig_3.png", width = 158, height = 93, units = "mm")

## Verificação de Normalidade para
## idade das marcas e avaliação de ousadia

shapiro.test(data$Age)
shapiro.test(data$`Q7_1 - Daring`)

## Teste de Correlação de Postos de Spearman

cor.test(data$Age,
         data$`Q7_1 - Daring`, method = 'spearman',
         alternative = c("less")) #solução final

## IC(99%) para o Coeficiente de Correlação de Postos de Spearman

rho_spearman <- -0.3758505
n <- 697

se <- sqrt((1+(((rho_spearman)^2)/2))/(n-3)) #Bonnett e Wright (2000)

Zl <- 0.5*(log(1+rho_spearman)-log(1-rho_spearman)) - (qnorm(0.01/2)*se) #alpha = 0.01
Zu <- 0.5*(log(1+rho_spearman)-log(1-rho_spearman)) + (qnorm(0.01/2)*se) #alpha = 0.01

Ls <- (exp(2*Zl)-1)/(exp(2*Zl)+1) #Limite superior do IC

Li <- (exp(2*Zu)-1)/(exp(2*Zu)+1) #Limite inferior do IC

IC <- c(Li, Ls)

##### Analise do topico 3 ----

### Objetivo: Quantas marcas se encaixam em cada uma das categoriais?

## tabela2
tabela2 <- data %>% group_by(Category) %>% summarise(Quantidade_marcas = n())

tabela2$Category <- as.numeric(tabela2$Category)


tabela2$Category <- cut(tabela2$Category, 16, labels =c(
  "Beauty products",
  "Beverages",
  "Cars",
  "Children's products",
  "Clothing products",
  "Department stores",
  "Financial services",
  "Food and dining",
  "Health products and services",
  "Home design and decoration",
  "Household Products",
  "Media and entertainment",
  "Sports and hobbies",
  "Technology products and stores",
  "Telecommunications","Travel services"))

tabela2$Porcentagem <- round((tabela2$Quantidade_marcas/sum(tabela2$Quantidade_marcas))*100, 2)

tabela2 #solução final

#write.table(arrange(tabela2,desc(Quantidade_marcas)), file="tabela2.xlsx")

##### Analise do topico 4 ----

### Objetivo: Relacao entre risco em caso de decisao errada e importancia da decisão

## Dispersão (grafico 4)

ggplot(data) +
  aes(x = `Q2: importance of decision`, y = `Q4: Risk in case of wrong decision`) +
  geom_point(colour = "#A11D21", size = 3) +
  labs(
    x = "Importância da Decisão",
    y = "Risco em Caso de Decisão Errada"
  ) +
  theme_estat()

#ggsave("fig_4.png", width = 158, height = 93, units = "mm")

## Verificação de Normalidade para
## Relação entre risco em caso de decisão errada e importância da decisão

shapiro.test(data$`Q2: importance of decision`)
shapiro.test(data$`Q4: Risk in case of wrong decision`)

## Teste de Correlação de Postos de Spearman

cor.test(data$`Q2: importance of decision`,
         data$`Q4: Risk in case of wrong decision`, method = 'spearman',
         alternative = c("greater")) #solução final

## IC(99%) para o Coeficiente de Correlação de Postos de Spearman

rho_spearman <- 0.828469
n <- 697

se <- sqrt((1+(((rho_spearman)^2)/2))/(n-3)) #Bonnett e Wright (2000)

Zl <- 0.5*(log(1+rho_spearman)-log(1-rho_spearman)) - (qnorm(0.01/2)*se) #alpha = 0.01
Zu <- 0.5*(log(1+rho_spearman)-log(1-rho_spearman)) + (qnorm(0.01/2)*se) #alpha = 0.01

Ls <- (exp(2*Zl)-1)/(exp(2*Zl)+1) #Limite superior do IC

Li <- (exp(2*Zu)-1)/(exp(2*Zu)+1) #Limite inferior do IC

IC <- c(Li, Ls)

##### Analise do topico 5 (erro)  ----

### Objetivo: Comparacao entre a qualidade da marca e sua visibilidade

summary(data$Visibility)
summary((data$Visibility)*25) #rescaling to 0-100

summary(data$High_quality_pct) #qualidade da marca

tabela3 <- data.frame(cbind(data$High_quality_pct, (data$Visibility)*25))

colnames(tabela3) <- c("Faixa Qualidade", "Visibilidade")

tabela3$`Faixa Qualidade` <- cut(data$High_quality_pct, 4, labels = c("Menor Qualidade",
                                              "Qualidade Mediana",
                                              "Alta Qualidade",
                                              "Mais Alta Qualidade"))



## Dispersao

ggplot(data) +
  aes(x = High_quality_pct, y = Visibility*25) +
  geom_point(colour = "#A11D21", size = 3) +
  labs(
    x = "Qualidade da Marca",
    y = "Visibilidade"
  ) +
  theme_estat()



## Grafico BoxPlot

ggplot(drop_na(tabela3)) +
  aes(x = `Faixa Qualidade`, y = Visibilidade) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
  ) +
  labs(x = "Faixa de Qualidade da Marca", y = "Visibilidade") +
  theme_estat()


## ANOVA

ajuste<-aov(tabela3$Visibilidade~tabela3$`Faixa Qualidade`)

summary(ajuste) #tabela ANOVA
TukeyHSD(ajuste,ordered=T,conf.level=0.95) #comparacoes multiplas

##### Analise do topico 5 (certo) ----

### Objetivo: Comparacao entre a qualidade da marca e sua visibilidade

## grafico de setor (grafico 5)

contagem <- data %>% 
  group_by(Premium) %>%
  summarise(Freq = n()) %>%
  mutate(Prop = round(100*(Freq/sum(Freq)), 2)) %>%
  arrange(desc(Premium)) %>%
  mutate(posicao = cumsum(Prop) - 0.5*Prop)

ggplot(contagem) +
  aes(x = factor(""), y = Prop , fill = factor(Premium)) +
  geom_bar(width = 1, stat = "identity") +
  coord_polar(theta = "y") +
  geom_text(
    aes(x = 1.8, y = posicao, label = paste0(Prop, "%")),
    color = "black"
  ) +
  theme_void() +
  theme(legend.position = "top") +
  scale_fill_manual(values = cores_estat2, name = 'Qualidade da Marca')

#ggsave("fig_5.png", width = 158, height = 93, units = "mm")

## Boxplot (grafico 6)

ggplot(data) +
  aes(x = Premium, y = Visibility) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
  ) +
  labs(x = "Qualidade da Marca", y = "Visibilidade") +
  theme_estat()

#ggsave("fig_6.png", width = 158, height = 93, units = "mm")


## ANOVA

ajuste<-aov(data$Visibility~data$Premium)

summary(ajuste) #tabela ANOVA
TukeyHSD(ajuste,ordered=T,conf.level=0.95) #comparacoes multiplas



##### Analise do topico ADICIONAL ----

### Objetivo: Relacao entre o tipo do produto e sua qualidade

## grafico de colunas (grafico 7)

prod_prem <- data %>%
  group_by(Premium, Product) %>%
  summarise(freq = n())

f <- 0
g <- c()
for (i in prod_prem$freq){
  f <- f+1
  if (f <= 3){
    g <- c(g, i/(85+82+6))
  }
  
  if (f >3 & f <= 6){
    g <- c(g, i/(203+136+13))
  }
  
  if(f >6){
    g <- c(g, i/(78+91+3))
  }
  
}

prod_prem$freq_relativa <- round(g*100, 2)

porcentagens <- str_c(prod_prem$freq_relativa, "%") %>% str_replace("\\.", ",")

legendas <- str_squish(str_c(prod_prem$freq, " (", porcentagens, ")"))

ggplot(prod_prem) +
  aes(
    x = fct_reorder(Premium, freq, .desc = T), y = freq,
    fill = Product, label = legendas
  ) +
  geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, hjust = 0.5,
    size = 2.2
  ) +
  labs(x = "Qualidade da Marca", y = "Frequ�ncia") +
  theme_estat()

#ggsave("fig_7.png", width = 158, height = 93, units = "mm")


## Contingency table e Qui-quadrado de independencia

# Comando para tabela cruzada:

tab2 <- xtabs(~ Product + Premium, data = data)

# Calcular as medidas de associa��o da tabela:

summary(assocstats(tab2))
